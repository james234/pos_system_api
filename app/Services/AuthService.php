<?php

namespace App\Services;

use App\Exceptions\CustomException;
use App\Interfaces\ILoginlogs;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;

class AuthService
{
    private $user;

    private $log;

    private $current_time;

    public function __construct(User $user, ILoginlogs $log)
    {
        $this->user = $user;
        $this->log = $log;
        $this->current_time = Carbon::now();
    }

    public function Admintoken($username, $password)
    {
        $user = $this->user->where('username', $username)->first();

        if(is_null($user)){
            return (object)[
                'status'=> 0,
                'message'=> 'Invalid credentials'
            ];
        }

        if($user->status == 0){
            return (object)[
                'status'=>0,
                'message'=> 'User currently disabled contact admin'
            ];
        }

        if($user->role_id != "super-admin"){
            return (object)[
                'status'=>0,
                'message'=> 'Access denied'
            ];
        }

        $check_password = $this->password_check($user->password, $password);

        if($check_password == false){

            return (object)[
                'status'=> 0,
                'message'=> 'Invalid credentials'
            ];
        }
        DB::beginTransaction();

        try {
            $generate_token = $this->generate_token("admin routes", $user, "admin-routes");
            $this->log->create([
                'user_id'=> $user->id,
                'login_datetime'=> $this->current_time,
                'ip_address'=> Request::ip() ?? " ",
                'agent'=> Request::header('user-agent') ?? " ",
                'response'=> "Login successfull"
            ]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            throw new CustomException($ex->getMessage());
        }


        $data = [
            'access_token'=> $generate_token->token,
            'token_type'=> "Bearer",
            'expires_at'=> $generate_token->expires_at,
            'user'=> [
                'name'=> $user->first_name . " ". $user->last_name,
                'username'=> $user->username,
                'role'=> $user->role_id
            ],
        ];
        return (object)[
            'data'=> $data,
            'status'=> 1
        ];

    }

    public function Shoptoken($username, $password)
    {
        $user = $this->user->where('username', $username)->first();

        if(is_null($user)){
            return (object)[
                'status'=> 0,
                'message'=> 'Invalid credentials'
            ];
        }

        if($user->status == 0){
            return (object)[
                'status'=>0,
                'message'=> 'User currently disabled contact admin'
            ];
        }

        $check_password = $this->password_check($user->password, $password);

        if($check_password == false){

            return (object)[
                'status'=> 0,
                'message'=> 'Invalid credentials'
            ];
        }


        DB::beginTransaction();

        try {
            $generate_token = $this->generate_token("shop token", $user, "api");
            $this->log->create([
                'user_id'=> $user->id,
                'login_datetime'=> $this->current_time,
                'ip_address'=> Request::ip() ?? " ",
                'agent'=> Request::header('user-agent') ?? " ",
                'response'=> "Login successfull"
            ]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            throw new CustomException($ex->getMessage());
        }

        $data = [
            'access_token'=> $generate_token->token,
            'token_type'=> "Bearer",
            'expires_at'=> $generate_token->expires_at,
            'user'=> [
                'name'=> $user->first_name . " ". $user->last_name,
                'username'=> $user->username,
                'role'=> $user->role_id
            ],
        ];
        return (object)[
            'data'=> $data,
            'status'=> 1
        ];
    }

    public function warehouseToken($username, $password)
    {
        $user = $this->user->where('username', $username)->first();

        if(is_null($user)){
            return (object)[
                'status'=> 0,
                'message'=> 'Invalid credentials'
            ];
        }

        if($user->status == 0){
            return (object)[
                'status'=>0,
                'message'=> 'User currently disabled contact admin'
            ];
        }

        if($user->role_id == "staff"){
            return (object)[
                'status'=>0,
                'message'=> 'Access denied'
            ];
        }

        $check_password = $this->password_check($user->password, $password);

        if($check_password == false){

            return (object)[
                'status'=> 0,
                'message'=> 'Invalid credentials'
            ];
        }



        DB::beginTransaction();

        try {
            $generate_token = $this->generate_token("warehouse token", $user, "warehouse-routes");
            $this->log->create([
                'user_id'=> $user->id,
                'login_datetime'=> $this->current_time,
                'ip_address'=> Request::ip() ?? " ",
                'agent'=> Request::header('user-agent') ?? " ",
                'response'=> "Login successfull"
            ]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            throw new CustomException($ex->getMessage());
        }

        $data = [
            'access_token'=> $generate_token->token,
            'token_type'=> "Bearer",
            'expires_at'=> $generate_token->expires_at,
            'user'=> [
                'name'=> $user->first_name . " ". $user->last_name,
                'username'=> $user->username,
                'role'=> $user->role_id
            ],
        ];
        return (object)[
            'data'=> $data,
            'status'=> 1
        ];
    }

    private function password_check($hashValue, $password)
    {
        if(!Hash::check($password, $hashValue)){
            return false;
        }
        return true;
    }

    private function generate_token($description,$user, $scope)
    {
        $token = $user->createToken($description, ["{$scope}"])->accessToken;

        //$expiration = $user->createToken($description, ["{$scope}"])->token->expires_at->diffInSeconds(Carbon::now());

        return (object)[
            'token'=> $token,
            'expires_at'=> Carbon::now()->addHours(24)
        ];
    }

    public function logout($user)
    {
        DB::beginTransaction();
        try {
            $user->token()->revoke();
            $this->log->updateItem(['user_id'=> $user->id], [
                'logout_datetime'=> $this->current_time
            ]);
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollBack();
            throw new CustomException($ex->getMessage());
        }

        return true;
    }
}
