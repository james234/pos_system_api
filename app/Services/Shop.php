<?php

namespace App\Services;

use App\Interfaces\IOutlet;
use App\Interfaces\IProductVariants;

class Shop
{
    protected $outlet, $product_variants;

    public function __construct(IOutlet $outlet, IProductVariants $product_variants)
    {
        $this->outlet = $outlet;
        $this->product_variants = $product_variants;
    }

    public function shop_products($shop_id)
    {
        $products =  collect($this->outlet->avaliable_products($shop_id));

        $value = $products->map(function($item){

            // if($item->product_image == null){
            //     $item->product_image =
            // }

            if($item->has_variance == 1){

                $item->variant = $this->product_variants->variants_and_prices($item->product_id);
            }

            unset($item->product_id);
            return $item;

        });

        return $value;
    }

    public function products()
    {
        $products = collect( $this->outlet->products());

        $value = $products->map(function($item){

            if($item->has_variance == 1){

                $item->variants = $this->product_variants->variants_and_prices($item->product_id);
            }

            unset($item->product_id);
            return $item;

        });

        return $value;
    }
}
