<?php

use App\Interfaces\IoutletProduct;

class StockUpdater{

    protected $outlet_product;
    public function __construct(IoutletProduct $outlet_product)
    {
        $this->outlet_product = $outlet_product;
    }


    public function outlet_product_stock_update(array $products)
    {
        //value - outlet_id, product_id's, quantities

        foreach ($products as $product) {
            # code...
        }
    }

    public function outlet_product_variant_stock_update(array $products)
    {
        //values - outlet_id,product_variant_id,quantity
        foreach ($products as $product) {
            # code...
        }
    }


    public function warehose_stock_update(int $warehouse_id, array $product_id)
    {
        //values - warehouse id, product_id's
        foreach ($product_id as $id) {
            # code...
        }
    }





}
