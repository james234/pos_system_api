<?php

namespace App\Services;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class ImageService
{

    public function resize_image(object $image, $upload_path,$width,$height, $file_name)
    {
        $path = storage_path("app/".trim($upload_path, "/"));
        $filename = Str::slug($file_name);
        $image_name = $filename."-pos.".$image->getClientOriginalExtension();
        $img = Image::make($image->getRealPath());
        $img->resize($width,$height, function(object $constraint){

        })->save($path.'/'.$image_name);
        return (object)[
            "relative_url"=> trim($upload_path,"/")."/".$image_name,
        ];
    }
}
