<?php

namespace App\Services;

use App\Interfaces\IoutletProduct;
use App\Interfaces\IoutputProduct;
use App\Interfaces\IProduct;
use App\Interfaces\IProductVariants;
use App\Repos\Outlet;
use Illuminate\Support\Facades\Auth;

class TransactionService
{

    protected $product, $variant, $errors, $amount_paid, $payment_type, $outlet_product, $outlet, $outlet_product_stock, $outlet_product_variant_stock,$total_price;

    public function __construct(IProduct $product, IProductVariants $variant, IoutletProduct $outlet_product, Outlet $outlet)
    {
        $this->product = $product;
        $this->variant = $variant;
        $this->outlet_product = $outlet_product;
        $this->outlet = $outlet;

    }


    public function process_transaction($request)
    {

        $auth_user = Auth::user();

        $this->amount_paid = $request->get('amount_paid');
        $this->payment_type = $request->get('payment_type');


        $products = $request->products;

        foreach ($products as $product) {

            $value = (object)$product;
            //dd($value->serial_no);

            if(isset($value->variant_identifier)){

                //process with variants
                $progress = $this->product_with_variants($value);

                if($progress->status == 1){
                    $this->total_price[] = $progress->selling_price;
                }else{
                    $this->errors[] = [
                        'product_serial_no'=> $progress->product_serial_no,
                        'reason'=> $progress->reason
                    ];
                }
            }else{

                //process base without variants
                $progress = $this->product_without_variants($value);

                if($progress->status == 1){
                    $this->total_price[] = $progress->selling_price;
                }else{
                    $this->errors[] = [
                        'product_serial_no'=> $progress->product_serial_no,
                        'reason'=> $progress->reason
                    ];
                }

            }
            // $product = $this->check_product_exists($value->serial_no); //check if order product exists

            // if($product === false){

            //     $this->errors[] = [
            //         'product_serial_no'=> $value->serial_no,
            //         'reason'=> 'Product not found'
            //     ];


            // }

            // if(isset($value->variant_identifier)){

            //     $variant = $this->check_variant_exits($value->variant_identifier); //check if product exists

            //     if($variant === false){
            //         $this->errors[] = [
            //             'product_serial_no'=> $value->serial_no,
            //             'variant_id'=> $value->variant_identifier,
            //             'reason'=> "No variant found for product"
            //         ];
            //     }

            // }


            // $product_stock = $this->check_out_stock($auth_user->outlet_id,$product->id, $value->quantity);



        }



        if(array_sum($this->total_price) > $this->amount_paid){
            return handle_response((object)[
                'status'=> 0,
                'message'=> 'Transaction failed',
                'error'=> 'Invalid amount passed'
            ]);
        }

        if(!is_null($this->errors)){
            return handle_response((object)[
                'status'=> 0,
                'message'=> 'Transaction Failed',
                'error'=> $this->errors
            ]);
        }


        return handle_response((object)[
            'status'=> 1,
            'message'=> "Transaction successfull",
            'data'=> [
                'invoice_no'=> "LO7772-K938BS",
                'balance'=> 0
            ]
        ]);
    }



     /**
     * process products with variant
     *
     * @param object $product
     * @return object
     */
    private function product_with_variants(object $product)
    {


    }

     /**
     * process products without variant
     *
     * @param object $product
     * @return object
     */
    private function product_without_variants(object $product)
    {
        $validate_product = $this->check_product_exists($product->serial_no);

        $auth_user = Auth::user();

        if($validate_product === false){

            return [
                'product_serial_no'=> $product->serial_no,
                'reason'=> 'Product not found',
                'status'=> 0
            ];

        }

        //check out of stock

        $stock = $this->check_out_stock($auth_user->outlet_id,$validate_product->id, $product->quantity);

        if($stock === false){
            return [
                'product_serial_no'=> $product->serial_no,
                'reason'=> 'Product out of stock',
                'status'=> 0
            ];
        }

        //check selling price
        if($product->selling_price <= $validate_product->purchase_price){

            return [
                'product_serial_no'=> $product->serial_no,
                'reason'=> 'Invalid selling price passed',
                'status'=> 0
            ];

        }

        return (object)[
            'selling_price'=> $product->selling_price,
            'status'=> 1
            //'current_outlet'=> $stock->outlet_product->stock - $product->quantity,

        ];


    }

    /**
     * check if product order exists
     * @param int serial_no
     *
     */
    private function check_product_exists(int $serial_no)
    {
        $product = $this->product->findItem(['serial_no'=> $serial_no]);

        if(is_null($product)){
            return false;
        }

        return $product;
    }

     /**
     * check if variant order exists
     * @param string variant identifier
     *
     */
    private function check_variant_exits(string $variant_identifier)
    {
        $variant = $this->variant->findItem(['product_variant_identifer'=> $variant_identifier]);

        if(is_null($variant)){
            return false;
        }

        return $variant;
    }

    private function check_out_stock(int $outlet_id, int $product_id, $quantity)
    {
        //$product = $this->product->findItem()
        $outlet_product = $this->outlet_product->findItem(['product_id'=> $product_id], ['outlet_id'=> $outlet_id]);

        $outlet =  $this->outlet->findItem(['id'=> $outlet_id]);


        if($outlet_product->stock < $quantity){
            return false;
        }


        return (object)[
            'outlet'=> $outlet,
            'outlet_product'=> $outlet_product
        ];
    }





}
