<?php

namespace App\Interfaces;

interface IBase
{
    public function create(array $data);
    public function insertGetId(array $data);
    public function findItem(array $condition);
    public function updateItem(array $condition, array $data);
    public function getAll();
    public function getItem(array $condition, array $selects);
    public function getItems(array $condition, array $selects);
    public function selectAll(array $selects);
    public function selectItem(array $condition, array $selects);
    public function deleteItem(array $condition);
}
