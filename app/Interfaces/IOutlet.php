<?php

namespace App\Interfaces;

interface IOutlet extends IBase
{
    public function products();
    public function avaliable_products($outlet_id);
}
