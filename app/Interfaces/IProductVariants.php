<?php

namespace App\Interfaces;

interface IProductVariants extends IBase
{
    public function variants_and_prices($product_id);
}
