<?php


namespace App\Traits;


use Illuminate\Support\Facades\Cookie;

trait TokenTrait
{
    public function getAccessToken()
    {
        return 'Bearer ' . Cookie::get('api_token');
    }
}
