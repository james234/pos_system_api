<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function (AccessDeniedHttpException $exception) {

           if(($exception->getCode()==403  || $exception->getCode()==0)  && getenv("APP_DEBUG"))
         {
             return response()->json([
                 "message"=>"Access forbidden",
                 "status"=>0,
                 "status_code"=>401
             ],401);
         }

        });

        $this->renderable(function(MethodNotAllowedHttpException $exception){

            if(($exception->getCode() == 405  || $exception->getCode()==0) && getenv("APP_DEBUG")){
                return response()->json([
                    "message"=>$exception->getMessage(),
                    "status"=>0,
                    "status_code"=>405
                ],405);
            }
        });

        $this->renderable(function(NotFoundHttpException $exception){

            if(($exception->getCode() == 404 || $exception->getCode()==0) && getenv("APP_DEBUG")){
                    return response()->json([
                        "message"=>"Route not found",
                        "status"=>0,
                        "status_code"=>404
                    ],404);
            }
        });

        $this->renderable(function(CustomException $exception){

            $code = $exception->getStatusCode();
            
            return response()->json([
                "message"=> $exception->getMessage(),
                'status'=> 0,
                "status_code"=> $code
            ], $code);
        });


    }
}
