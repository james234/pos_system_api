<?php


if(!function_exists('handle_response')){

    function handle_response($response){

        $status = $response->status ;

        switch ($status) {
            case 1:
                return (object)[
                    'message' => $response->message ?? "success",
                    'status' => 1,
                    'data' => $response->data ?? null
                ];

            case 0:
                return (object)[
                    'message' => $response->message ?? "failed",
                    'error' =>  $response->error ?? $response->errorMessage ?? null,
                    'status' => 0
                ];

            default:
            return (object)[
                'message' => $response->message,
                'status' => $response->status
            ];
        }

    }
}
