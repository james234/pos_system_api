<?php

namespace App\Providers;

use App\Interfaces\ILoginlogs;
use App\Interfaces\IOutlet;
use App\Interfaces\IoutletProduct;
use App\Interfaces\IProduct;
use App\Interfaces\IProductVariants;
use App\Interfaces\IRole;
use App\Interfaces\IVariants;
use App\Repos\Loginlog;
use App\Repos\Outlet;
use App\Repos\outletProduct;
use App\Repos\Product;
use App\Repos\productVariants;
use App\Repos\Role;
use App\Repos\Variants;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(IRole::class, Role::class);
        $this->app->singleton(ILoginlogs::class,Loginlog::class);
        $this->app->singleton(IOutlet::class, Outlet::class);
        $this->app->singleton(IProductVariants::class, productVariants::class);
        $this->app->singleton(IVariants::class, Variants::class);
        $this->app->singleton(IProduct::class, Product::class);
        $this->app->singleton(IoutletProduct::class, outletProduct::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
