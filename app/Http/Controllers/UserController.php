<?php

namespace App\Http\Controllers;

use App\Interfaces\IRole;
use App\Models\User;
use App\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    protected $user, $role, $authService;

    public function __construct(User $user, IRole $role, AuthService $authService)
    {
        $this->user = $user;
        $this->role = $role;
        $this->authService = $authService;
    }

    public function test()
    {
        echo "test";
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name'=> 'required|string',
            'last_name'=> 'required|string',
            'email'=> 'required|unique:users,email',
            'phone'=> 'required|numeric',
            'role'=> 'required|string',
            'username'=> 'required|string|unique:users,username',
            'password'=> 'required|confirmed'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors()->first(), $validator->errors());
        }

        $role = $this->role->findItem(['role_slug'=> $request->get('role')]);

        if(is_null($role)){
            return $this->sendError('Invalid role passed');
        }

        if($role->status == 0){
            return $this->sendError('Role not active contact admin');
        }

        $store = $this->user->create([
            'first_name'=> $request->get('first_name'),
            'last_name'=> $request->get('last_name'),
            'email'=> $request->get('email'),
            'phone'=> $request->get('phone'),
            'password'=> bcrypt($request->get('password')),
            'username'=> $request->get('username'),
            'role_id'=> $role->id
        ]);

        if($store){
            return $this->sendResponse('','New User added successfully');
        }

    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'=> 'required|string',
            'password'=> 'required'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors()->first(), $validator->errors());
        }

        $auth = $this->authService->Shoptoken($request->get('username'), $request->get('password'));

        if($auth->status == 0){
            return $this->sendError($auth->message);
        }

        Cookie::queue('api_token',$auth->data->access_token, 480);
        Cookie::queue('username', $auth->data->username, 480);
        Cookie::queue('role', $auth->data->role, 480);

        return $this->sendResponse($auth->data, 'login successfull');

    }

    public function logout()
    {
        $authuser = Auth::user();

        $logout = $this->authService->logout($authuser);


        if($logout === true){

            Cookie::queue(Cookie::forget('api_token'));
            Cookie::queue(Cookie::forget('username'));
            Cookie::queue(Cookie::forget('role'));
            
            return $this->sendResponse('','Logout successfull');
        }

    }
}
