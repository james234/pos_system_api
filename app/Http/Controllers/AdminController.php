<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function test()
    {
        echo "test";
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'=> 'required|string',
            'password'=> 'required'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors()->first(), $validator->errors());
        }

        $auth = $this->authService->Admintoken($request->get('username'), $request->get('password'));

        if($auth->status == 0){
            return $this->sendError($auth->message);
        }

        return $this->sendResponse($auth->data, 'login successfull');
    }
}
