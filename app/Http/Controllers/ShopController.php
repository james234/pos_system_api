<?php

namespace App\Http\Controllers;

use App\Services\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    public $shop;

    private $auth;

    public function __construct(Shop $shop)
    {
        $this->shop = $shop;
        //$this->auth = Auth::user();
    }


    public function shop_available_products()
    {
        $user = Auth::user();

        if($user->role_id == "super-admin"){
            $shop = $this->shop->products();
        }else{
            $shop = $this->shop->shop_products($user->outlet_id);
        }


        return $this->sendResponse($shop);
    }
}
