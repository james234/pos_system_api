<?php

namespace App\Http\Controllers;

use App\Services\TransactionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    public $transaction;

    public function __construct(TransactionService $transactionService)
    {
        $this->transaction = $transactionService;
    }

    public function buy(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'products'=> 'required|array',
            'products.*'=> 'required|array',
            'products.*.serial_no'=> 'required|integer',
            'products.*.variant_identifier'=> 'nullable|string',
            'products.*.selling_price'=> 'nullable|integer',
            'products.*.quantity'=> 'required|integer',
            // 'has_variance'=> 'required|integer',
            // 'product_variant_identifer'=> 'required_if:has_variance,1',
            //'selling_price'=> 'nullable|integer',
            'amount_paid'=> 'required|integer',
            'payment_type'=> 'required|string'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors()->first(), $validator->errors());
        }

        $handle_transaction = $this->transaction->process_transaction($request);

        if($handle_transaction->status == 0){
            return $this->sendError($handle_transaction->message, $handle_transaction->error ?? " ");
        }

        return $this->sendResponse($handle_transaction->data ?? " ", $handle_transaction->message);




    }
}
