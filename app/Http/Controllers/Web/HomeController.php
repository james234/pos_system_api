<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {
        return view('index');
    }

    public function dashboard()
    {
        return view('dashboard.index');
    }

    public function shop()
    {
        return view('dashboard.shop');
    }

    public function get_product()
    {
        return view('partials.product');
    }
}
