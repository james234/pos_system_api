<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CurrentRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $current = url()->current();
        $current_route = Cookie::get('current_route');

        if(!is_null($current_route)){
            Cookie::queue(Cookie::forget('current_route'));
            Cookie::queue('current_route', $current, 480);
        }else{
            Cookie::queue('current_route', $current, 480);
        }

        return $next($request);
    }
}
