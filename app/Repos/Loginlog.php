<?php

namespace App\Repos;

use App\Interfaces\ILoginlogs;

class Loginlog extends Base implements ILoginlogs
{

    public function __construct($table_name="login_logs")
    {
        parent::__construct($table_name);
    }
}
