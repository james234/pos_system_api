<?php

namespace App\Repos;

use App\Interfaces\IExample;
use App\Interfaces\IOutlet;
use Illuminate\Support\Facades\DB;

class Outlet extends Base implements IOutlet
{
    public $table_name;

    protected $pivot_table = "outlet_product";

    public function __construct($table_name="outlets")
    {
        parent::__construct($table_name);
        $this->table_name = $table_name;
    }

    public function products()
    {
        return DB::table($this->pivot_table)
                         ->where("{$this->pivot_table}.stock", ">", 0)
                        // ->where("{$this->pivot_table}.outlet_id", $outlet_id)
                        ->leftJoin('products as p','p.id','=', "{$this->pivot_table}.product_id")
                        ->leftJoin("{$this->table_name}", "{$this->table_name}.id", '=', "{$this->pivot_table}.outlet_id")
                        ->select( "{$this->table_name}.name as outlet_name", "p.id as product_id", 'p.product_name','p.image as product_image','p.serial_no','p.has_variance', 'p.created_at')
                        ->get();
    }

    public function avaliable_products($outlet_id)
    {
        return DB::table($this->pivot_table)
                        ->where("{$this->pivot_table}.outlet_id", $outlet_id)
                        ->where("{$this->pivot_table}.stock", ">", 0)
                        ->leftJoin('products as p','p.id','=', "{$this->pivot_table}.product_id")
                        ->leftJoin("{$this->table_name}", "{$this->table_name}.id", '=', "{$this->pivot_table}.outlet_id")
                        ->select("{$this->table_name}.name as outlet_name", "p.id as product_id", 'p.product_name','p.image as product_image','p.serial_no','p.has_variance', 'p.created_at')
                        ->get();

    }
}
