<?php

namespace App\Repos;

use App\Interfaces\IProduct;

class Product extends Base implements IProduct
{

    public function __construct($table_name="products")
    {
        parent::__construct($table_name);
    }
}
