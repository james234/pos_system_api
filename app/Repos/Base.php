<?php

namespace App\Repos;

use App\Interfaces\IBase;
use Illuminate\Support\Facades\DB;

class Base implements IBase
{

    public $table_name;

    public function __construct($table_name)
    {
        $this->table_name = $table_name;
    }

    public function create(array $data)
    {
        return DB::table($this->table_name)->insert($data);
    }

    public function insertGetId(array $data)
    {
        return DB::table($this->table_name)->insertGetId($data);
    }

    public function findItem(array $condition)
    {
        return DB::table($this->table_name)->where($condition)->first();
    }

    public function updateItem(array $condition, array $data)
    {
        return DB::table($this->table_name)->where($condition)->update($data);
    }

    public function getAll()
    {
        return DB::table($this->table_name)->orderByDesc('created_at')->get();
    }

    public function getItem(array $condition, array $selects)
    {
        return DB::table($this->table_name)->where($condition)->select($selects)->orderByDesc('created_at')->first();
    }

    public function getItems(array $condition, array $selects)
    {
        return DB::table($this->table_name)->where($condition)->select($selects)->orderByDesc('created_at')->get();
    }

    public function selectAll(array $selects)
    {
        return  DB::table($this->table_name)->select($selects)->orderByDesc('created_at')->get();
    }

    public function selectItem(array $condition, array $selects)
    {
        return  DB::table($this->table_name)->select($selects)->orderByDesc('created_at')->first();
    }

    public function deleteItem(array $condition)
    {
        return DB::table($this->table_name)->where($condition)->delete();
    }
}
