<?php

namespace App\Repos;

use App\Interfaces\IRole;

class Role extends Base implements IRole
{

    public function __construct($table_name="roles")
    {
        parent::__construct($table_name);
    }
}
