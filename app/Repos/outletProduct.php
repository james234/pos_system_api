<?php

namespace App\Repos;

use App\Interfaces\IoutletProduct;

class outletProduct extends Base implements IoutletProduct
{

    public function __construct($table_name="outlet_product")
    {
        parent::__construct($table_name);
    }
}
