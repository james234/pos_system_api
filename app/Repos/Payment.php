<?php

namespace App\Repos;

use App\Interfaces\IPayment;

class Payment extends Base implements IPayment
{
    public $table_one = "outlet_product";
    public $table_two = "outlet_product_variants";


    public function __construct($table_name="table")
    {
        parent::__construct($table_name);
    }

    
}
