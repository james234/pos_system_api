<?php

namespace App\Repos;

use App\Interfaces\IExample;

class Example extends Base implements IExample
{

    public function __construct($table_name="table")
    {
        parent::__construct($table_name);
    }
}
