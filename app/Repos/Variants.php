<?php

namespace App\Repos;

use App\Interfaces\IVariants;

class Variants extends Base implements IVariants
{
    public function __construct($table_name="variants")
    {
        parent::__construct($table_name);
    }
}
