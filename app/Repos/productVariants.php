<?php

namespace App\Repos;

use App\Interfaces\IProductVariants;
use Illuminate\Support\Facades\DB;

class productVariants extends Base implements IProductVariants
{

    public $table_name;

    public function __construct($table_name="product_variant")
    {
        parent::__construct($table_name);
        //$this->table_name = $table_name;
    }

    public function variants_and_prices($product_id)
    {
        return DB::table($this->table_name)
                    ->where("{$this->table_name}.product_id", $product_id)
                    ->leftJoin("variants as v", 'v.id', '=', "{$this->table_name}.variant_id")
                    ->select("{$this->table_name}.product_variant_identifer","{$this->table_name}.image as product_variant_image", "{$this->table_name}.min_sale_price", "{$this->table_name}.max_sale_price")
                    ->get();
    }
}
