/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*********************************!*\
  !*** ./resources/js/utility.js ***!
  \*********************************/
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

window.getFormAsJsonData = function (form_element) {
  var data = {};
  $.map($("".concat(form_element, " input[type=\"text\"],").concat(form_element, " select,\n            ").concat(form_element, " input[type=\"number\"],\n            ").concat(form_element, " input[type=\"radio\"]:checked")), function (elem, idx) {
    if ($(elem).attr("type") === "radio") {
      var name = $(elem).attr("name");
      data = _objectSpread(_objectSpread({}, data), {}, _defineProperty({}, name, $("input[type=radio][name=".concat(name, "]:checked")).val()));
    } else {
      data = _objectSpread(_objectSpread({}, data), {}, _defineProperty({}, $(elem).attr("id"), $(elem).val()));
    }
  });
  return data;
};

window.getFormAsFormData = function (form_element) {
  var formData = new FormData();
  $.map($("".concat(form_element, " input,").concat(form_element, " select")), function (elem, idx) {
    if ($(elem).attr("type") === "file") {
      formData.append($(elem).attr("id"), $(elem)[0].files[0]);
    } else {
      formData.append($(elem).attr("id"), $(elem).val());
    }
  });
  return formData;
};

window.redirectTo = function (loginUrl) {
  var timeout = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  if (timeout === true) {
    setTimeout(function () {
      window.location.href = loginUrl;
    }, 1000);
  } else {
    window.location.href = loginUrl;
  }
};

window.fireAlert = function (_ref) {
  var title = _ref.title,
      icon = _ref.icon,
      text = _ref.text,
      html = _ref.html;
  Swal.fire({
    icon: icon !== null && icon !== void 0 ? icon : "success",
    title: title,
    text: text,
    html: html,
    button: {
      text: "Close",
      value: true,
      visible: true,
      className: "btn btn-alert-btn"
    }
  });
};

window.toggleFullPageLoader = function (show) {
  if (show !== undefined) {
    if (show === true) {
      $('#full-loader').removeClass("d-none");
    } else {
      $('#full-loader').addClass("d-none");
    }
  } else {
    $('#full-loader').toggleClass("d-none");
  }
};

window.conditionalAlertfire = function (_ref2) {
  var title = _ref2.title,
      icon = _ref2.icon,
      text = _ref2.text,
      action_url = _ref2.action_url,
      method = _ref2.method,
      proof = _ref2.proof,
      table_id = _ref2.table_id,
      proof2 = _ref2.proof2;
  Swal.fire({
    icon: icon !== null && icon !== void 0 ? icon : "info",
    title: title,
    text: text,
    button: {
      text: "Close",
      value: true,
      visible: true,
      className: "btn btn-alert-btn"
    },
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: "Yes ".concat(proof)
  }).then(function (result) {
    if (result.value) {
      $.ajax({
        url: action_url,
        type: method,
        processData: false,
        contentType: false,
        beforeSend: function beforeSend() {
          toggleFullPageLoader(true);
        },
        success: function success(data, status, xhr) {
          fireAlert({
            title: "Success",
            text: "".concat(proof2, " successfully")
          });
          $("#".concat(table_id)).DataTable().ajax.reload();
        },
        error: function error(jqXhr, textStatus, errorMessage) {
          var data = jqXhr.responseJSON;
          var errors = data.errors || data.message || data.data || data;

          if (jqXhr.status == 401) {
            redirectTo('../');
          }

          fireAlert({
            icon: "error",
            title: "Request Failed!",
            text: errors
          }); //  if (typeof errors === "object") {
          //      Object.keys(errors.data).forEach(function(key){
          //          let selector = $(`#${key}-error-msg`);
          //          selector.html(errors[key][0]);
          //          $(`#${key}`).parent('.form-group').removeClass("has-danger");
          //          selector.fadeIn();
          //      });
          //  } else {
          //  }
        },
        complete: function complete() {
          toggleFullPageLoader(false);
        }
      });
    }
  });
};
/******/ })()
;