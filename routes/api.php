<?php

use App\Http\Controllers\ShopController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test', function(){
    return url()->current();
});

Route::post('register', [UserController::class, 'register']);

Route::prefix('shop')->group(function(){
    Route::post('/login', [UserController::class, 'login']);

    Route::middleware('auth:api', 'scopes:api')->group(function(){

        Route::get('/test', [UserController::class, 'test']);
        Route::get('/products', [ShopController::class, 'shop_available_products']);

        //buy products
        Route::post('/make/purchase', [TransactionController::class, 'buy']);

        Route::get('/user', function(){

            $user = [
                'username'=> Auth::user()->username,
            ];

            return response()->json($user);
        });

        //logout
        Route::post('/logout', [UserController::class, 'logout'])->name('logout');
    });



});




