<?php

use App\Http\Controllers\UserController;
use App\Http\Controllers\WarehouseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('/login', [WarehouseController::class, 'login']);

Route::middleware('auth:warehouse-routes', 'scopes:warehouse-routes')->group(function(){
        Route::get('/test', [WarehouseController::class, 'test']);
        //Route::get('/test', [UserController::class, 'test']);
        Route::get('/products', [ShopController::class, 'shop_available_products']);
        Route::post('/logout', [UserController::class, 'logout']);
});




