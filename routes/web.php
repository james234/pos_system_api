<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', function(){
//     return view('Login');
// })->name('login');


// Route::get('/index/{any?}', function(){
//     return view('index');
// })->where('any', '.*');

//login route

Route::get('shop/login', 'Web\AuthController@login')->name('login');

Route::get('shop/dashboard', 'Web\HomeController@dashboard')->name('shop.dashboard');
Route::get('shop/pos', 'Web\HomeController@shop')->name('shop.pos');
Route::get('get/product', 'Web\HomeController@get_product')->name('load.product');
//other routes for logined in user

// Route::get('/index/{any?}', [Web\HomeController::class, 'index'])->where('any', '.*');
// [UserController::class, 'register']
