<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebitorsListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debitors_lists', function (Blueprint $table) {
            $table->id();
            $table->foreignid('customer_id')->references('id')->on('customers');
            $table->foreignId('invoice_id')->references('id')->on('invoices');
            $table->decimal('total_amount_owing', 8,2);
            $table->decimal('total_amount_paid', 8,2);
            $table->decimal('total_amount_left', 8,2);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debitors_lists');
    }
}
