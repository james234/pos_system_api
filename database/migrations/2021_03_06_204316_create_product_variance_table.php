<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProductVarianceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variant', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('product_id', false, true);
            $table->foreign('product_id')->references('id')->on('products');
            $table->bigInteger('variant_id', false, true);
            $table->foreign('variant_id')->references('id')->on('variants');
            $table->string('image')->default('default.png');
            $table->double('purchase_price', 13,2);
            $table->double('min_sale_price', 13,2);
            $table->double('max_sale_price', 13,2);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'))->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variance');
    }
}
