//const { default: axios } = require("axios");

const base_url =`http://localhost:8000/api/shop/`;
const admin_url = `http://localhost:8000/api/admin/`;
const warehouse = `http://localhost:8000/api/warehouse/`;
const access_token = localStorage.getItem("access_token");

axios.defaults.baseURL = base_url;
axios.defaults.headers.common["Authorization"] = `Bearer ${access_token}`;
axios.defaults.headers.post["Accept"] = "application/json";
axios.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

axios.interceptors.request.use(

    // (request) => {
    //     if(request.url.includes("application")){
    //         const access_token = localStorage.getItem("access_token");
    //         request.header.common["Authorization"] = `Bearer ${access_token}`
    //     }


    //     return request;

    // },
    // (error)=> {
    //     return Promise.reject(error);
    // }
)
