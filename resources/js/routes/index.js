import products from '../Pages/products.vue'
export default {
    mode : 'history',
    routes : [

        {
            path: '/index',
            name: 'home',
            component: products,
        }
    ]
}
