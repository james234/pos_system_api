@extends('layouts.master')

@push('page_css')
    <link rel="stylesheet" href="{{asset('vendors/jqvmap/jqvmap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/flag-icon-css/css/flag-icon.min.css')}}">
    {{-- <link rel="stylesheet" href="{{asset('perx-css/new.css')}}"> --}}
@endpush

@push('searcher')
    <ul class="navbar-nav mr-lg-2">

        <li class="nav-item nav-search d-none d-lg-block">
            <div class="input-group" style="width: 80%;">
                <div class="input-group-prepend">
                <span class="input-group-text" id="search">
                    <i class="mdi mdi-magnify"></i>
                </span>
                </div>
                <input type="text" class="form-control" placeholder="Search for anything..." aria-label="search" aria-describedby="search">
            </div>
        </li>
    </ul>
@endpush
@section('content')

      <div class="row">

        <div class="col-12 col-lg-8 grid-margin sigma-rows">

            {{-- Products --}}

            <div class="row">

                <div class="col-md-4">
                    <div class="card card_pointer get_product" data-url="{{route('load.product')}}">
                        <img src="{{asset('images/product-1.jpg')}}" alt="" class="card-img-top">
                        <div class="card-body">

                            <h4 class="card-title">Card title</h4>
                            <p class="card-text-light">N500</p>

                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card card_pointer">
                        <img src="{{asset('images/first_image.jpg')}}" alt="" class="card-img-top">
                        <div class="card-body">

                            <h4 class="card-title">Card title</h4>
                            <p class="card-text-light">N500</p>

                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card card_pointer">
                        <img src="{{asset('images/first_image.jpg')}}" alt="" class="card-img-top">
                        <div class="card-body">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text-light">N500</p>

                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card card_pointer">
                        <img src="{{asset('images/first_image.jpg')}}" alt="" class="card-img-top">
                        <div class="card-body">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text-light">N500</p>


                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card card_pointer">
                        <img src="{{asset('images/first_image.jpg')}}" alt="" class="card-img-top">
                        <div class="card-body">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text-light">N500</p>


                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card card_pointer">
                        <img src="{{asset('images/first_image.jpg')}}" alt="" class="card-img-top">
                        <div class="card-body">
                            <h4 class="card-title">Card title</h4>
                            <p class="card-text-light">N500</p>


                        </div>
                    </div>
                </div>


            </div>



        </div>

        <div class="col-6 col-md-4 col-lg-4 grid-margin">

            <div class="pos-sidebar" id="pos-sidebar">
                <div class="pos-sidebar-header">

                    <div class="title">Customer Order</div>
                </div>
            </div>
                {{-- <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            Current Order
                            <button class="btn btn-info " style="margin-left:11rem;">Clear All</button>
                        </h4>

                        <div class="row">
                            <div class="col-md-2">
                                <label for="">Productdfdfdfvdfvdfvdfvdfvdvdfvdfdfdfdfvdfvdfvdfvdfvdfvdfvdfdfvdfvdf</label>
                            </div>
                        </div>

                    </div>
                </div> --}}
        </div>
    </div>

    <div class="modal modal-pos-item fade" id="question_form" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document" id="form_questions" style="max-width:70%;">

        </div>
    </div>
@endsection

@push('page_scripts')

<script src="{{asset('vendors/jquery.flot/jquery.flot.js')}}"></script>
<script src="{{asset('vendors/jquery.flot/jquery.flot.pie.js')}}"></script>
<script src="{{asset('vendors/jquery.flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('vendors/jquery.flot/jquery.flot.crosshair.js')}}"></script>
<script src="{{asset('vendors/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('vendors/jqvmap/maps/jquery.vmap.world.js')}}"></script>
<script src="{{asset('vendors/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<script src="{{asset('vendors/peity/jquery.peity.min.js')}}"></script>
<script src="{{asset('js/jquery.flot.dashes.js')}}"></script>
<script src="{{mix('/assets/pages/pos_handler.js')}}"></script>

@endpush
