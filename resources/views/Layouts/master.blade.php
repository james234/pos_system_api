<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>POS-system</title>

    @once

        <link rel="stylesheet" href="{{asset('vendors/mdi/css/materialdesignicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.base.css')}}">


    @endonce
    <!-- base:css -->


    @stack('page_css')
    @once
        <link rel="stylesheet" href="{{asset('css/vertical-layout-light/style.css')}}">
        <link rel="stylesheet" href="{{asset('perx-css/main.css')}}">
    @endonce


</head>
<body class="sidebar-dark">


    <div id="full-loader" class="loader-demo-box d-none">
        <div class="circle-loader"></div>
      </div>

    <div class="container-scroller">

        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">

            <div class="text-center navbar-brand-wrapper d-flex align-items-center">
                <a class="navbar-brand brand-logo" href="{{route('shop.dashboard')}}"><img src="{{asset('images/perx.png')}}" alt="logo" style="max-width:60%!important;"/></a>
                <a class="navbar-brand brand-logo-mini" href="{{route('shop.dashboard')}}"><img src="{{asset('images/perx-icon.png')}}" alt="logo"/></a>
            </div>

              <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">

                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="mdi mdi-menu"></span>
                  </button>



                  @if (request()->segment(2)=="pos")

                        @stack('searcher')


                  @endif


                  <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item dropdown">


                        <p class="welcome-back-text" style="margin-right:20px;">
                                 Welcome, <span class="first_name">{{Cookie::get('name')}}</span>
                             </p>
                 </li>

                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link" href="#" data-toggle="dropdown" id="profileDropdown">
                            <i style="font-size: 1.38rem;" class="mdi mdi-account"></i>
                           <div
                              style="
                          display: flex;
                          flex-direction: row;
                          justify-content: space-between;
                        "
                          >


                          </div>
                      </a>



                          <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                            <a href="" class="dropdown-item">
                                <i class="mdi mdi-account"></i>
                                Profile
                            </a>

                             <a href="" class="dropdown-item">
                                    <i class="mdi mdi-settings "></i>
                                    Change Password
                                </a>


                             <a href="javascript:void(0)" id="logout"
                                   class="dropdown-item">
                                    <i class="mdi mdi-logout"></i>
                                    Logout
                              </a>


                    </li>
                  </ul>

                  <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                  </button>




              </div>

        </nav>

        <div class="container-fluid page-body-wrapper">

            @include('partials.sidebar')

            <div class="main-panel">

                <div class="content-wrapper">
                    @yield('content')
                </div>

                @include('partials.footer')
            </div>
        </div>
    </div>


    <script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>

    @stack('page_scripts')

    <script src="{{asset('js/off-canvas.js')}}"></script>
  <script src="{{asset('js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('js/template.js')}}"></script>
  <script src="{{asset('js/settings.js')}}"></script>
  <script src="{{asset('js/todolist.js')}}"></script>

  <script src="{{asset("vendors/sweetalert2/sweetalert2.min.js")}}"></script>

  <script src="{{mix('/assets/pages/utility.js')}}"></script>

  <script>

      $('#logout').click(function(){

            alert('ok')
      })
  </script>

</body>
</html>
