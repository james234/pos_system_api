<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    {{-- <meta name="csrf-token" content="{{@csrf_token()}}" /> --}}

    <link rel="stylesheet" href="{{asset('vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.base.css')}}">

    <link rel="stylesheet" href="{{asset('css/vertical-layout-light/style.css')}}">
    <link rel="stylesheet" href="{{asset("perx-css/main.css")}}">
    <style>

         .auth form .form-group {
                margin-bottom: 0;
            }
      </style>

    @include('scripts')

</head>
<body id="body-ground">

    <div id="full-loader" class="loader-demo-box d-none">
        <div class="circle-loader"></div>
    </div>

    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div style="backgroud: transparent;" class="content-wrapper d-flex align-items-center auth px-0">
                <div class="row w-100 mx-0">
                    <div class="col-lg-4 mx-auto">
                        <div class="auth-form-light text-left py-5 px-4 px-sm-5 border">
                            <div class="brand-logo">
                                <img src="{{asset('images/logo.png')}}" alt="logo" style="width:74px; !important;">
                            </div>
                              <h4>Hello! let's get started</h4>
                                    <h6 class="font-weight-light">Sign in to continue.</h6>
                                    <form class="pt-3" id="login-form" action="">
                                    <div class="form-group">
                                        <div id="server-error-msg" class="error  text-danger"></div>

                                         <div id="server-success-msg" class="" style="color:green;"></div>
                                        <input type="text" class="form-control form-control-lg monitor" id="email" placeholder="Enter your username">
                                        <label for="email" id="email-error-msg" class="error  text-danger"></label>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control form-control-lg monitor" id="password" placeholder="Password">
                                         <label id="password-error-msg" class="error  text-danger" for="password"></label>
                                    </div>
                                     <div class="">
                                        <button type="submit" style="background-color: #81C041; border: none;"
                                        class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">SIGN IN</button>
                                    </div>
                                    <div class="my-2 d-flex justify-content-between align-items-center">
                                        <a href="" class="auth-link text-black">Forgot password?</a>
                                    </div>

                                    </form>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>
    <script src="{{asset('customjs/utility.js')}}"></script>

    <script>

        $('#login-form').submit(function(e){

            e.preventDefault();

            let username = $('#email').val();
            let password = $('#password').val();


            if(email == ""){
           $('#password-error-msg').html("");
            $('#email-error-msg').html("Username field required");
					$('#email')
						.parent(".form-group")
						.removeClass("has-danger");
                    $('#email-error-msg').fadeIn();

            return false;
        }else if(password == ""){
              $('#email-error-msg').html("");
            $('#password-error-msg').html("Password field required");
					$("#password")
						.parent(".form-group")
						.removeClass("has-danger");
                     $('#password-error-msg').fadeIn();

            return false;
        }else{
             $('#email-error-msg').html("");
             $('#password-error-msg').html("");

             $.ajax({
                url: "/api/shop/login",
                type: "POST",
                data: {username, password},
                beforeSend: function(){
                    toggleFullPageLoader(true);
                },
                success: function(data){
                    //console.log(data)
                    if(data.status == 1){
                         $("#server-error-msg").html("");
                        $("#server-error-msg").fadeOut();
                        $("#server-success-msg").html("Login successfull");
                        $("#server-success-msg").fadeIn();

                    }
                },
                error: function(error){

                    const data = error.responseJSON;
                   // console.log(data)
                    const errors = data.errors || data.message || data.data || data;

                    if(data.status == 401){

                    }


                    if (typeof errors.data  === "object") {
                        Object.keys(errors).forEach(function(key) {
                            let selector = $(`#${key}-error-msg`);
                            selector.html(errors.data[key][0]);
                            $(`#${key}`)
                                .parent(".form-group")
                                .removeClass("has-danger");
                            selector.fadeIn();
                        });
                    }else if(typeof errors.email === "object"){
                        $("#server-success-msg").html("");
                        $("#server-success-msg").fadeOut();
                        $("#server-error-msg").html(errors.email[0]);
                        $("#server-error-msg").fadeIn();
                    }else {
                        $("#server-success-msg").html("");
                        $("#server-success-msg").fadeOut();
                        $("#server-error-msg").html(errors);
                        $("#server-error-msg").fadeIn();
                    }
                },
                complete: function(){
                    toggleFullPageLoader(false);
                }
             })


        }

        });
    </script>
</body>
</html>
