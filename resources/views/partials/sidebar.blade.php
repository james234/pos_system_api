<nav class="sidebar sidebar-offcanvas" id="sidebar" style="overflow-x:scroll">

    <ul class="nav">

        <li class="nav-item {{request()->is('/shop/dashboard') ? 'active': ''}}">
            <a class="nav-link" href="{{route('shop.dashboard')}}">
              <i class="mdi mdi-view-dashboard menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
        </li>


        <li class="nav-item {{request()->is('/shop/pos') ? 'active': ''}}">
            <a class="nav-link" href="{{route('shop.pos')}}">
              <i class="mdi mdi-cart menu-icon"></i>
              <span class="menu-title">Point of sales</span>
            </a>
        </li>


    </ul>

</nav>
