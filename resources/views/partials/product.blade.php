<div class="modal-content" style="border-radius:6px;">


    <div class="modal-body p-0">

        <a href="#" data-dismiss="modal" class="close">
            <i class="mdi mdi-close"></i>
        </a>

        <div class="pos-product">

            <div class="pos-product-img">
                <div class="img" style="background-image: url({{asset('images/product-1.jpg')}})"></div>
            </div>

            <div class="pos-product-info">

                <div class="title">Grill Chicken Chop</div>
                <div class="desc">
                    chicken, egg, mushroom
                </div>
                <div class="price">$10.99</div>
                <hr>

                <div class="option-row">
                    <div class="qty">
                        <div class="input-group">
                            <a href="#" class="btn btn-default">
                                <i class="mdi mdi-minus"></i>
                            </a>
                            <input type="text" class="form-control border-0 text-center" name value="1">
                            <a href="#" class="btn btn-default">
                                <i class="mdi mdi-plus"></i>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="option-row">
                    <div class="option-title">Size</div>
                    <div class="option-list">
                        <div class="option">
                            <input type="radio" id="size3" name="size" class="option-input" checked>
                            <label for="size3" class="option-label">
                                <span class="option-text">Small</span>
                                <span class="option-price">+0.00</span>
                            </label>
                        </div>

                        <div class="option">
                            <input type="radio" id="size1" name="size" class="option-input">
                            <label for="size1" class="option-label">
                                <span class="option-text">Large</span>
                                <span class="option-price">+3.00</span>
                            </label>
                        </div>

                        <div class="option">
                            <input type="radio" id="size2" name="size" class="option-input">
                            <label for="size2" class="option-label">
                                <span class="option-text">Medium</span>
                                <span class="option-price">+1.50</span>
                            </label>
                        </div>


                    </div>
                </div>

                <div class="option-row">
                    <div class="option-title">Add on</div>
                    <div class="option-list">

                    </div>
                </div>



                <div class="btn-row">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                    <a href="#" class="btn btn-success">
                        Add to cart
                        <i class="mdi mdi-plus"></i>
                    </a>
                </div>

            </div>
        </div>



    </div>


</div>
