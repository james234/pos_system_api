<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" href="/favicon.ico" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>SaleItPOS</title>
    <link rel="stylesheet" href="{{asset('vendors/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.base.css')}}">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="{{asset('vendors/jqvmap/jqvmap.min.css')}}">
  <link rel="stylesheet" href="{{asset('vendors/flag-icon-css/css/flag-icon.min.css')}}">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <!-- <link rel="stylesheet" href="/css/vertical-layout-light/style.css"> -->
  <link rel="stylesheet" href="{{asset('css/vertical-layout-light/style.css')}}">
  <!-- endinject -->
  {{-- <link rel="shortcut icon" href="/images/favicon.png" /> --}}
  <link rel="stylesheet" href="{{asset('perx-css/main.css')}}">
  </head>
  <body class="sidebar-dark">
    <div class="container-scroller">
      <div id="app">
          <home-component></home-component>
      </div>
    </div>


    <script src="{{asset('vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <script src="{{asset('vendors/jquery.flot/jquery.flot.js')}}"></script>
    <script src="{{asset('vendors/jquery.flot/jquery.flot.pie.js')}}"></script>
    <script src="{{asset('vendors/jquery.flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('vendors/jqvmap/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('vendors/jqvmap/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{asset('vendors/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
    <script src="{{asset('vendors/peity/jquery.peity.min.js')}}"></script>
    <script src="{{asset('js/jquery.flot.dashes.js')}}"></script>
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{asset('js/off-canvas.js')}}"></script>
    <script src="{{asset('js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('js/template.js')}}"></script>
    <script src="{{asset('js/settings.js')}}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>
